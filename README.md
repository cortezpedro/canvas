Canvas Tool

MatLab files and documentation for the Canvas class

Quick tips for using Canvas Tool:

There are two versions of the Canvas class. One for GUIDE, another for App Designer.
They can be found at source/GUIDE_Figs and source/AppDesigner, respectively.

Both versions are based on an abstract superclass Canvas, from which the subclasses
Canvas_2D (for 2D plots) and Canvas_3D (for 3D plots) are inherited. Each subclass
deals differently with the fit2view method.

Methods ax_onButtonDown, onButtonDown, onButtonUp, onMouseMove and onMouseScroll are
intentionally left blank on Canvas_2D and Canvas_3D, for users to implement on accordingly
to their needs. These methods may also be overridden on subclasses of Canvas_2D and Canvas_3D.

Users are encouraged to implement their own client subclass, inheriting from either
Canvas_2D or Canvas_3D, if any application-specific code is needed.

Simple Canvas usage examples are provided on the tests directory, on the useOfCanvas_Test*.m files.

To draw on the Matlab Axes widget using the Canvas class, two strategies are possible:

* Setting and 'update' function and calling it:

    canvas = Canvas_2D(); % Initialize a 2D canvas
    canvas.setUpdateFcn(@drawFcn); % Provide handle to function that draws on canvas
    canvas.update(); % Update canvas (Obs.: If drawFcn needs input parameters, they can be passed here)

* Getting the 'plotting context' (handle to Axes) from the Canvas:

    canvas = Canvas_3D(); % Initialize a 3D canvas
    where = canvas.getContext(); % Get canvas context
    plot3(where,[0,1],[2,1],[3,4]); % Plots a line in 3D

An example of the coupled use of Canvas and Emouse classes is provided on the mouse_events_app directory.
To run this code, go to the mouse_events_app directory and run main.m from the Matlab command prompt.

GitLab repository:

* Available at https://gitlab.com/cortezpedro/canvas