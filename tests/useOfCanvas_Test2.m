%% TEST 2
% Display geometric figures on canvas
%%
% Clear workspace
clear
close all
clc

% Add path to canvas folder
addpath('canvas');

% Initialize canvas
canvas = Canvas_2D();

% Turn grid on
canvas.Grid(true);

% Provide handle to function that updates the canvas
canvas.setUpdateFcn(@drawFcn_Test);

% Update canvas
canvas.update();