%% TEST 1
% Plot graphs on a canvas
%%
% Clear workspace
clear
close all
clc

% Add path to canvas folder
addpath('canvas');

% Initialize canvas
canvas = Canvas_2D();

% Disable canvas proportionality
canvas.Equal(false);

% Set 5% of free borders
canvas.setFreeBorder(0.05);

% Get canvas context (handle to where plots must be done)
where = canvas.getContext();

% Plot a random 2nd order parable
x = -5:0.1:5;
y = polyval(rand(1,3),x);
plot(where,x,y);

% Turn grid on
canvas.Grid(true);

% Fit plot on canvas
canvas.fit2view();