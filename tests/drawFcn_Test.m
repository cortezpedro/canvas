function drawFcn_Test(ax)

if nargin < 1
    % Get handle to axes
    ax = gca;
end

% Clean current axes
cla(ax)

% Get geometry
[x,y,id] = load_LESM_letters;

% Assemble colors matrix
clr = [0 0 1;
       1 0 0;
       0 1 0;
       1 1 0];
   
% Alpha channel value
a = 0.5;

% Plot outline
plot(ax,x,y,'color',[0 0 0]);

% Fill internal regions
for i = 1:size(id,1)
    fill(ax,x(id(i,1):id(i,2)),y(id(i,1):id(i,2)),clr(i,:),'FaceAlpha',a,...
        'Tag',sprintf('%i',i),'ButtonDownFcn',ax.ButtonDownFcn);
end
end