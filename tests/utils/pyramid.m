%%
%------------------------------------------------------------------
% Plots a pyramid with defined apex coordinates, height, base,
% orientaion, and color.
% This method is used to draw translation constraints on 3D models.
% Input arguments:
%  where: handle to canvas
%  X: apex coordinate on the X axis
%  Y: apex coordinate on the Y axis
%  Z: apex coordinate on the z axis
%  h: pyramid height
%  B: pyramid base
%  dir: pyramid pointing direction (x+, x-, y+, y-, z+, z-)
%  c: color (RGB vector)
function pyramid(where,X,Y,Z,h,B,dir,c,tag)
b = B/2;

% Check if direction is char or array of direction vectors
if ischar(dir)
    % Coodinate convertion
    if strcmp(dir,'x+')
        x = Y;
        y = Z;
        z = X;
    elseif strcmp(dir,'x-')
        x = Y;
        y = Z;
        z = X;
        h = -h;
    elseif strcmp(dir,'y+')
        x = Z;
        y = X;
        z = Y;
    elseif strcmp(dir,'y-')
        x = Z;
        y = X;
        z = Y;
        h = -h;
    elseif strcmp(dir,'z+')
        x = X;
        y = Y;
        z = Z;
    elseif strcmp(dir,'z-')
        x = X;
        y = Y;
        z = Z;
        h = -h;
    end
    
    % Base coordinates
    Xb = [x + b, x + b, x - b, x - b];
    Yb = [y - b, y + b, y + b, y - b];
    Zb = [z - h, z - h, z - h, z - h];
    
    % Face 1 coordinates
    Xf1 = [x, x + b, x + b];
    Yf1 = [y, y - b, y + b];
    Zf1 = [z, z - h, z - h];
    
    % Face 2 coordinates
    Xf2 = [x, x + b, x - b];
    Yf2 = [y, y + b, y + b];
    Zf2 = [z, z - h, z - h];
    
    % Face 3 coordinates
    Xf3 = [x, x - b, x - b];
    Yf3 = [y, y + b, y - b];
    Zf3 = [z, z - h, z - h];
    
    % Face 4 coordinates
    Xf4 = [x, x - b, x + b];
    Yf4 = [y, y - b, y - b];
    Zf4 = [z, z - h, z - h];
    
else % dir is array - compute 5 points coordinates
    
    % Get direction vectors
    dx = dir(1,:);
    dy = dir(2,:);
    dz = dir(3,:);
    
    % Pt1 - apex of the pyramid
    pt_1 = [X, Y, Z];
    
    % Pt2
    pt_2 = pt_1 - h * dx - b * dy - b * dz;
    
    % Pt3
    pt_3 = pt_2 + 2 * b * dz;
    
    % Pt4
    pt_4 = pt_3 + 2 * b * dy;
    
    % Pt5
    pt_5 = pt_4 - 2 * b * dz;
    
    % Base coordinates
    Xb = [pt_2(1), pt_3(1), pt_4(1), pt_5(1)];
    Yb = [pt_2(2), pt_3(2), pt_4(2), pt_5(2)];
    Zb = [pt_2(3), pt_3(3), pt_4(3), pt_5(3)];
    
    % Face 1 coordinates
    Xf1 = [pt_2(1), pt_3(1), pt_1(1)];
    Yf1 = [pt_2(2), pt_3(2), pt_1(2)];
    Zf1 = [pt_2(3), pt_3(3), pt_1(3)];
    
    % Face 2 coordinates
    Xf2 = [pt_3(1), pt_4(1), pt_1(1)];
    Yf2 = [pt_3(2), pt_4(2), pt_1(2)];
    Zf2 = [pt_3(3), pt_4(3), pt_1(3)];
    
    % Face 3 coordinates
    Xf3 = [pt_4(1), pt_5(1), pt_1(1)];
    Yf3 = [pt_4(2), pt_5(2), pt_1(2)];
    Zf3 = [pt_4(3), pt_5(3), pt_1(3)];
    
    % Face 4 coordinates
    Xf4 = [pt_5(1), pt_2(1), pt_1(1)];
    Yf4 = [pt_5(2), pt_2(2), pt_1(2)];
    Zf4 = [pt_5(3), pt_2(3), pt_1(3)];
    
end

% Draw pyramid
if nargin == 8
    if ischar(dir)
        if strcmp(dir,'z+') || strcmp(dir,'z-')
            fill3(where, Xb, Yb, Zb, c, 'EdgeColor', [0,0,0]);
            fill3(where, Xf1, Yf1, Zf1, c, 'EdgeColor', [0,0,0]);
            fill3(where, Xf2, Yf2, Zf2, c, 'EdgeColor', [0,0,0]);
            fill3(where, Xf3, Yf3, Zf3, c, 'EdgeColor', [0,0,0]);
            fill3(where, Xf4, Yf4, Zf4, c, 'EdgeColor', [0,0,0]);
            
        elseif strcmp(dir,'x+') || strcmp(dir,'x-')
            fill3(where, Zb, Xb, Yb, c, 'EdgeColor', [0,0,0]);
            fill3(where, Zf1, Xf1, Yf1, c, 'EdgeColor', [0,0,0]);
            fill3(where, Zf2, Xf2, Yf2, c, 'EdgeColor', [0,0,0]);
            fill3(where, Zf3, Xf3, Yf3, c, 'EdgeColor', [0,0,0]);
            fill3(where, Zf4, Xf4, Yf4, c, 'EdgeColor', [0,0,0]);
            
        elseif strcmp(dir,'y+') || strcmp(dir,'y-')
            fill3(where, Yb, Zb, Xb, c, 'EdgeColor', [0,0,0]);
            fill3(where, Yf1, Zf1, Xf1, c, 'EdgeColor', [0,0,0]);
            fill3(where, Yf2, Zf2, Xf2, c, 'EdgeColor', [0,0,0]);
            fill3(where, Yf3, Zf3, Xf3, c, 'EdgeColor', [0,0,0]);
            fill3(where, Yf4, Zf4, Xf4, c, 'EdgeColor', [0,0,0]);
        end
    else
        fill3(where, Xb, Yb, Zb, c, 'EdgeColor', [0,0,0]);
        fill3(where, Xf1, Yf1, Zf1, c, 'EdgeColor', [0,0,0]);
        fill3(where, Xf2, Yf2, Zf2, c, 'EdgeColor', [0,0,0]);
        fill3(where, Xf3, Yf3, Zf3, c, 'EdgeColor', [0,0,0]);
        fill3(where, Xf4, Yf4, Zf4, c, 'EdgeColor', [0,0,0]);
        hold on
    end
elseif nargin == 9
    if ischar(dir)
        if strcmp(dir,'z+') || strcmp(dir,'z-')
            fill3(where, Xb, Yb, Zb, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Xf1, Yf1, Zf1, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Xf2, Yf2, Zf2, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Xf3, Yf3, Zf3, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Xf4, Yf4, Zf4, c, 'EdgeColor', [0,0,0], 'tag', tag);
            
        elseif strcmp(dir,'x+') || strcmp(dir,'x-')
            fill3(where, Zb, Xb, Yb, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Zf1, Xf1, Yf1, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Zf2, Xf2, Yf2, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Zf3, Xf3, Yf3, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Zf4, Xf4, Yf4, c, 'EdgeColor', [0,0,0], 'tag', tag);
            
        elseif strcmp(dir,'y+') || strcmp(dir,'y-')
            fill3(where, Yb, Zb, Xb, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Yf1, Zf1, Xf1, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Yf2, Zf2, Xf2, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Yf3, Zf3, Xf3, c, 'EdgeColor', [0,0,0], 'tag', tag);
            fill3(where, Yf4, Zf4, Xf4, c, 'EdgeColor', [0,0,0], 'tag', tag);
        end
    else
        fill3(where, Xb, Yb, Zb, c, 'EdgeColor', [0,0,0], 'tag', tag);
        fill3(where, Xf1, Yf1, Zf1, c, 'EdgeColor', [0,0,0], 'tag', tag);
        fill3(where, Xf2, Yf2, Zf2, c, 'EdgeColor', [0,0,0], 'tag', tag);
        fill3(where, Xf3, Yf3, Zf3, c, 'EdgeColor', [0,0,0], 'tag', tag);
        fill3(where, Xf4, Yf4, Zf4, c, 'EdgeColor', [0,0,0], 'tag', tag);
    end
end
end