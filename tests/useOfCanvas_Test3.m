%% TEST 3
% Spatial drawings on 3D canvas
%%
% Clear workspace
clear
close all
clc

% Add path to canvas folder
addpath('canvas');

% Add path to utils folder
addpath('utils');

% Initialize canvas
canvas = Canvas_3D();

% Set -20% of free borders (zoom)
canvas.setFreeBorder(-0.2);

% Turn clipping off
canvas.Clipping(false);

% Turn grid on
canvas.Grid(true);

% Get canvas context (handle to where plots must be done)
where = canvas.getContext();

% Draw pyramids
pyramid(where,0,0,0,1,0.5,'x+',[0.7 0 0]);
pyramid(where,0,0,0,1,0.5,'x-',[0.7 0 0]);
pyramid(where,0,0,0,1,0.5,'y+',[0 0.7 0]);
pyramid(where,0,0,0,1,0.5,'y-',[0 0.7 0]);
pyramid(where,0,0,0,1,0.5,'z+',[0 0 0.7]);
pyramid(where,0,0,0,1,0.5,'z-',[0 0 0.7]);

% Fit plot on canvas
canvas.fit2view();