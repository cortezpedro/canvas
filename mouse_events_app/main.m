%% Main
%
%% Clear memory
clc;
clear;
close(findall(0,'Type','figure'));

% Add path to auxiliary folders
addpath('canvas','draw','gui','mouse');

%% Launch GUI
GUI_CanvasMouseEvents;      % ATTENTION: Calling .mlapp, not (code_file).m
%GUI_CanvasMouseEvents_Code; % Calling (code_file).m