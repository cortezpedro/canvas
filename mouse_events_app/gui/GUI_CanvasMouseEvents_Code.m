%%
% ### ATTENTION ###
% This is a copy of the code implemented on the GUI_CanvasMouseEvents.mlapp
% file. This serves only for quick access to the code, so it can be viewed
% without the need to open the App Designer environment.
%
% All changes to the program must be performed on the .mlapp file, then
% copied and pasted here, so this file is up to date. Any modifications
% made here will not affect the application.
%
% This file is currently not being called at any point of the
% CanvasMouseEvents application.
%--------------------------------------------------------------------------
classdef GUI_CanvasMouseEvents_Code < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        CanvasMouseEvents  matlab.ui.Figure
        ax_1               matlab.ui.control.UIAxes
        ax_2               matlab.ui.control.UIAxes
    end

    
    properties (Access = private)
        mouseEvents = []; % Handle to an object of the Emouse class
        canvas1     = []; % Handle to an object of the Canvas class
        canvas2     = []; % Handle to an object of the Canvas class
    end
    
    methods (Access = private)
        
        % Called when axes graphical component is resized
        function Canvas1_SizeChangedFcn(app,~)
            if ~isempty(app.canvas1)
                left   = -1;  right = 11;
                bottom = -1;  top   = 5;
                app.canvas1.fit2view([left, bottom, right, top]);
            end
        end
        
        % Called when axes graphical component is resized
        function Canvas2_SizeChangedFcn(app,~)
            if ~isempty(app.canvas2)
                left   = -1;  right = 11;
                bottom = -1;  top   = 5;
                app.canvas2.fit2view([left, bottom, right, top]);
            end
        end
        
        % Called when user attempts to close dialog
        function deleteCallback(app)
            choice = questdlg('Are you sure you want to exit?',...
            'Exit','Exit','Cancel','Cancel');
            switch choice
                case 'Cancel'
                    return
            end
            app.delete();
        end
    end
    

    % Callbacks that handle component events
    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            % Initialize objects to manage canvases behavior
            app.canvas1 = MyCanvas_2D(app.ax_1,app.CanvasMouseEvents);
            app.canvas2 = MyCanvas_2D(app.ax_2,app.CanvasMouseEvents);
            
            % Set uiaxes resize function
            app.ax_1.SizeChangedFcn = app.createCallbackFcn(@Canvas1_SizeChangedFcn, true);
            app.ax_2.SizeChangedFcn = app.createCallbackFcn(@Canvas2_SizeChangedFcn, true);
            
            % Turn rulers and grid off
            app.canvas1.Ruler(false); app.canvas1.Grid(false);
            app.canvas2.Ruler(false); app.canvas2.Grid(false);
            
            % Turn off axes tool bars
            app.ax_1.Toolbar.Visible = 'off';
            app.ax_1.Interactions = [];
            app.ax_2.Toolbar.Visible = 'off';
            app.ax_2.Interactions = [];
            
            % Set limits of canvas 1 and canvas 2 (xmin = -1, xmax = 11, ymin = -1, ymax = 5)
            left   = -1;  right = 11;
            bottom = -1;  top   = 5;
            app.canvas1.fit2view([left, bottom, right, top]);
            app.canvas2.fit2view([left, bottom, right, top]);
            
            % Initialize object to manage mouse events
            app.mouseEvents = MouseEvents(app.CanvasMouseEvents,app.canvas1);
            
            % Set deletion callback
            app.CanvasMouseEvents.CloseRequestFcn = app.createCallbackFcn(@deleteCallback, false);
        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create CanvasMouseEvents and hide until all components are created
            app.CanvasMouseEvents = uifigure('Visible', 'off');
            app.CanvasMouseEvents.Color = [0 0 0];
            app.CanvasMouseEvents.Position = [100 100 642 573];
            app.CanvasMouseEvents.Name = 'CanvasMouseEvents';

            % Create ax_1
            app.ax_1 = uiaxes(app.CanvasMouseEvents);
            app.ax_1.XColor = 'none';
            app.ax_1.YColor = 'none';
            app.ax_1.BackgroundColor = [0 0 0];
            app.ax_1.Position = [16 283 602 278];

            % Create ax_2
            app.ax_2 = uiaxes(app.CanvasMouseEvents);
            app.ax_2.XColor = 'none';
            app.ax_2.YColor = 'none';
            app.ax_2.BackgroundColor = [0 0 0];
            app.ax_2.Position = [16 6 602 278];

            % Show the figure after all components are created
            app.CanvasMouseEvents.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = GUI_CanvasMouseEvents_Code

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.CanvasMouseEvents)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.CanvasMouseEvents)
        end
    end
end