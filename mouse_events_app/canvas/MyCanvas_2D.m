%% MyCanvas_2D class
%
% This is a sub-class of the Canvas class, to deal especifically with axes
% that present only 2D behavior, on the CanvasMouseEvents educational app.
%
%% Authors
%%%
% * Pedro Cortez F Lopes (pedrocortez@id.uff.br)
%%%
% * Rafael Lopez Rangel (rafaelrangel@tecgraf.puc-rio.br)
%%%
% * Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
%% History
% @version 1.00
%
% Initial version: April 2020
%%%
% Adapted from the code initially prepared for the course CIV 2801 -
% Fundamentos de Computação Gráfica, 2019, second term, Department of Civil
% Engineering, PUC-Rio.
%
%% Class definition
classdef MyCanvas_2D < Canvas_2D
    %% Private properties - mouse events
    properties (SetAccess = private, GetAccess = private)
        firstPosition = [];         % x and y coordinates of the first pointer position.
        firstPositionGiven = false; % flag indicating that first point was selected.
        lineDisplayed = false;      % flag indicating that a line was displayed.
        clr = [0 0 1];              % color to be used for plots (blue)
        markSize = 0.01;            % radius of circle mark.
        xLim_mSz = 150;             % Proportionality constant (xLim/markSize)
        
        % handles to plots (for quick access)
        h_mark_1 = [];
        h_mark_2 = [];
        h_line   = [];
        
        draw = []; % handle to object that draws on canvas
    end
    %% Constructor method
    methods
        function this = MyCanvas_2D(ax,fig,fcn)
            if (nargin == 0)
                ax  = [];
                fig = [];
                fcn = [];
            elseif (nargin <= 2)
                fcn = [];
            end
            this = this@Canvas_2D(ax,fig,fcn);
            
            % Creates an object to draw on canvas
            this.draw = DrawOnCanvas(this);
        end
    end
    %% Public methods
    % Implementation of the methods that act as mouse event callbacks.
    methods
        %------------------------------------------------------------------
        % Mouse button down callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        % * whichMouseButton = 'left', 'right', 'center', 'double click'
        function onButtonDown(this,pt,whichMouseButton)
            % Compute mark size, based on canvas x limits
            this.markSize = diff(this.ax.XLim) / this.xLim_mSz;
            
            %%%%%%% COMPLETE HERE - MOUSEVENTS: 01 %%%%%%%
            switch whichMouseButton
                case 'left'
                    % Clear canvas
                    this.Clean();
                    
                    % Reinitialize handles to graphic objects
                    this.h_mark_1 = []; this.h_mark_2 = [];  this.h_line = [];
                    
                    % Draw mark for fisrt point and store handle to it
                    this.h_mark_1 = this.draw.circleMark(pt(1),pt(2),this.markSize, this.clr);
                    
                    % Store first position coordinates
                    this.firstPosition = pt;
                    
                    % Set boolean flag
                    this.firstPositionGiven = true;
%               case 'right'
%
%               case 'center'
%
%               case 'double click'
%
            end
            %%%%%%% COMPLETE HERE - MOUSEVENTS: 01 %%%%%%%
        end
        
        %------------------------------------------------------------------
        % Mouse button up callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        % * whichMouseButton = 'left', 'right', 'center', 'double click'
        function onButtonUp(this,~,~)
        %%%%%%% COMPLETE HERE - MOUSEVENTS: 03 %%%%%%%
            % Set boolean flag
            this.firstPositionGiven = false;
        %%%%%%% COMPLETE HERE - MOUSEVENTS: 03 %%%%%%%
        end
        
        %------------------------------------------------------------------
        % Mouse move callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        function onMouseMove(this,pt)
        %%%%%%% COMPLETE HERE - MOUSEVENTS: 02 %%%%%%%
            % Check if mouse is collecting second point
            if this.firstPositionGiven
                
                % Check if second point is different from first
                if all(this.firstPosition(1:2,1) ~= pt(1:2,1))
                    
                    % At each movement, delete graphical objects that
                    % composed previous frame
                    delete(this.h_mark_2); delete(this.h_line);
                    
                    % Draw next frame and store handles to new graphical
                    % objects
                    this.h_line = this.draw.elasticLine(this.firstPosition(1),this.firstPosition(2), ...
                                                        pt(1),pt(2), this.clr);
                    this.h_mark_2 = this.draw.circleMark(pt(1),pt(2),this.markSize, this.clr);
                end
            end
        %%%%%%% COMPLETE HERE - MOUSEVENTS: 02 %%%%%%%
        end
    end
end