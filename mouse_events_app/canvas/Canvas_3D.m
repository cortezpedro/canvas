%% Canvas_3D class
%
% This is a sub-class of the Canvas class, to deal especifically with axes
% that present 3D behavior.
%
%% Authors
%%%
% * Pedro Cortez F Lopes (pedrocortez@id.uff.br)
%%%
% * Rafael Lopez Rangel (rafaelrangel@tecgraf.puc-rio.br)
%%%
% * Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
%% History
% @version 1.00
%
% Initial version: September 2019
%%%
% Initially prepared for the course CIV 2801 - Fundamentos de Computa��o
% Gr�fica, 2019, second term, Department of Civil Engineering, PUC-Rio.
%
%% Class definition
classdef Canvas_3D < Canvas
    %% Constructor method
    methods
        function this = Canvas_3D(ax,fig,fcn)
            if (nargin == 0)
                ax  = [];
                fig = [];
                fcn = [];
            elseif (nargin <= 2)
                fcn = [];
            end
            this = this@Canvas(ax,fig,fcn);
        end
    end
    %% Public methods
    % Implementation of the abstract methods declared in super-class Canvas.
    methods
        %------------------------------------------------------------------
        % Fits axis limits to display everything drawn on canvas
        function fit2view(this,bBox)
            % Check if a bounding box was already provided
            if (nargin == 2)
                if ~isempty(bBox)
                    % Get x and y vectors
                    x = [bBox(1),bBox(4)]; x = [min(x) max(x)];
                    y = [bBox(2),bBox(5)]; y = [min(y) max(y)];
                    z = [bBox(3),bBox(6)]; z = [min(z) max(z)];
                    
                    % Set limits and return
                    axes(this.ax);
                    this.ax.XLim = x;
                    this.ax.YLim = y;
                    this.ax.ZLim = z;
                    
                    % Set flag to indicate that bounding box was provided
                    this.bBoxWasSet = true;
                    return;
                end
            end
            
            % Get list of handles to graphic objects drawn on this.ax
            plots = this.ax.Children;
            
            % If there is nothing drawn on this.ax, return
            if isempty(plots)
                view(this.ax,3);
                this.ax.XLim = [-1,1];
                this.ax.YLim = [-1,1];
                this.ax.YLim = [-1,1];
                return
            end
            
            % Get maximum and minimum coordinates
            if ~strcmp(plots(1).Type,'text')
                xmax = max(max(plots(1).XData));
                xmin = min(min(plots(1).XData));
                ymax = max(max(plots(1).YData));
                ymin = min(min(plots(1).YData));
                zmax = max(max(plots(1).ZData));
                zmin = min(min(plots(1).ZData));
            else
                xmax = max(plots(1).Position(1));
                xmin = min(plots(1).Position(1));
                ymax = max(plots(1).Position(2));
                ymin = min(plots(1).Position(2));
                zmax = max(plots(1).Position(3));
                zmin = min(plots(1).Position(3));
            end
            for i = 2:length(plots)
                if ~strcmp(plots(1).Type,'text')
                    xmax = max([xmax,max(max(plots(i).XData))]);
                    xmin = min([xmin,min(min(plots(i).XData))]);
                    ymax = max([ymax,max(max(plots(i).YData))]);
                    ymin = min([ymin,min(min(plots(i).YData))]);
                    zmax = max([zmax,max(max(plots(i).ZData))]);
                    zmin = min([zmin,min(min(plots(i).ZData))]);
                else
                    xmax = max([xmax,max(max(plots(i).Position(1)))]);
                    xmin = min([xmin,min(min(plots(i).Position(1)))]);
                    ymax = max([ymax,max(max(plots(i).Position(2)))]);
                    ymin = min([ymin,min(min(plots(i).Position(2)))]);
                    zmax = max([zmax,max(max(plots(i).Position(3)))]);
                    zmin = min([zmin,min(min(plots(i).Position(3)))]);
                end
            end
            
            % Adjust axes limits based on max and min coords 
            Lx = xmax - xmin;
            Mx = (xmax + xmin) * 0.5;
            xmin = Mx - (0.5 + this.freeBorder/2) * Lx;
            xmax = Mx + (0.5 + this.freeBorder/2) * Lx;
            
            Ly = ymax - ymin;
            My = (ymax + ymin) * 0.5;
            ymin = My - (0.5 + this.freeBorder/2) * Ly;
            ymax = My + (0.5 + this.freeBorder/2) * Ly;
            
            Lz = zmax - zmin;
            Mz = (zmax + zmin) * 0.5;
            zmin = Mz - (0.5 + this.freeBorder/2) * Lz;
            zmax = Mz + (0.5 + this.freeBorder/2) * Lz;
            
            % Reset axes limits
            this.ax.XLim = [xmin,xmax];
            this.ax.YLim = [ymin,ymax];
            this.ax.YLim = [zmin,zmax];
            
            % Set flag to indicate that bounding box was provided
            this.bBoxWasSet = true;
        end
        %------------------------------------------------------------------
        % Mouse button down callback - related to axes object
        % Input:
        % * this  = handle to this canvas object
        % * obj   = handle to axes associated to canvas
        % * event = struct with event data
        function ax_onButtonDown(~,~,~)
        end
        
        %------------------------------------------------------------------
        % Mouse button down callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        % * whichMouseButton = 'left', 'right', 'center', 'double click'
        function onButtonDown(~,~,~)
        end
        
        %------------------------------------------------------------------
        % Mouse button up callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        % * whichMouseButton = 'left', 'right', 'center', 'double click'
        function onButtonUp(~,~,~)
        end
        
        %------------------------------------------------------------------
        % Mouse move callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        function onMouseMove(~,~)
        end
        
        %------------------------------------------------------------------
        % Mouse scroll callback - generic callback, called from an
        % Emouse object.
        % Input:
        % * this  = handle to this canvas object
        % * pt    = current cursor coordinates
        function onMouseScroll(~,~)
        end
    end
end