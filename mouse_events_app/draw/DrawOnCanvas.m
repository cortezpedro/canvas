%% DrawOnCanvas Class
classdef DrawOnCanvas < handle
    %% Properties
    properties (SetAccess = private, GetAccess = private)
        canvas = []; % handle to a canvas object
    end
    %% Constructor / Destructor
    methods
        %------------------------------------------------------------------
        function this = DrawOnCanvas(canvas)
            this.canvas = canvas;
        end
        %------------------------------------------------------------------
        function kill(this)
            this.canvas = [];
            delete(this);
        end
    end
    %% Public methods
    methods
        %------------------------------------------------------------------
        % Draws a line from first to second point, using given color.
        function h = elasticLine(this,x1,y1,x2,y2,c)
            X = [x1 x2];
            Y = [y1 y2];
            h = plot(this.canvas.getContext, X, Y, 'Color', c);
        end
        
        %------------------------------------------------------------------
        % Plots a circle with defined center coordinates, radius and color.
        % This method is used to draw hinges on 2D models.
        % Input arguments:
        %  x: center coordinate on the X axis
        %  y: center coordinate on the Y axis
        %  r: circle radius
        %  c: color (RGB vector)
        function h = circleMark(this,x,y,r,c)
            circ = 0 : pi/20 : 2*pi;
            xcirc = x + r * cos(circ);
            ycirc = y + r * sin(circ);
            h = plot(this.canvas.getContext,xcirc, ycirc, 'color', c);
        end
    end
end